# Import flask and template operators
from flask import Flask, render_template, redirect, url_for,request
from flask.ext.compress import Compress
import os


class ReverseProxied(object):
    '''Wrap the application in this middleware and configure the
    front-end server to add these headers, to let you quietly bind
    this to a URL other than / and to an HTTP scheme that is
    different than what is used locally.

    In nginx:
    location /myprefix {
        proxy_pass http://192.168.0.1:5001;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Scheme $scheme;
        proxy_set_header X-Script-Name /myprefix;
        }

    :param app: the WSGI application
    '''
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        script_name = environ.get('HTTP_X_SCRIPT_NAME', '')
        if script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]

        scheme = environ.get('HTTP_X_SCHEME', '')
        if scheme:
            environ['wsgi.url_scheme'] = scheme
        server = environ.get('HTTP_X_FORWARDED_SERVER', '')
        if server:
                 environ['HTTP_HOST'] = server
        return self.app(environ, start_response)

# Define the WSGI application object
app = Flask(__name__)
app.wsgi_app = ReverseProxied(app.wsgi_app)
app.config.from_object(__name__)

Compress(app)
app.config['COMPRESS_DEBUG'] = True

app.config['UPLOAD_FOLDER'] = 'uploads'
app.config["MONGODB_SETTINGS"] = {'DB': "listview"}
app.config["SECRET_KEY"] = "c39dcb77e31b57bb094e816c4982ac3c"

##Below configurations need to be changed in the server.
app.config["VIS25_DB"] = "62aeffc3c2dc09ed5ef4391bfaf9738c"
app.config["SITE_DOMAIN"] = "127.0.0.1:5000"

UPLOAD_FOLDER = os.path.join('.', os.path.abspath(os.path.join(__file__, '..')), 'upload/')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024



# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return 'This route does not exist {}'.format(request.url), 404


#from app.data.controllers import mod_data as mod_data
from app.list.controllers import mod_list as mod_list
from app.list.controllers import mod_no_list as mod_no_list
# Register blueprint(s)
#app.register_blueprint(mod_data)
app.register_blueprint(mod_list)
app.register_blueprint(mod_no_list)
